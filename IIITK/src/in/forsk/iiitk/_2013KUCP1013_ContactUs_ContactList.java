package in.forsk.iiitk;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class _2013KUCP1013_ContactUs_ContactList extends AppCompatActivity implements _2013KUCP1013_ContactUs_ContactListAdapter.ItemClickListener{

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private _2013KUCP1013_ContactUs_ContactListAdapter adapter;
    private List<_2013KUCP1013_ContactUs_ContactListWrapper> list;
    static String jsonData;

    private RelativeLayout toolbarHolder;
    private ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list_2013kucp1013_contactus_);

        getInitialize();

    }

    private void getInitialize() {

        list = new ArrayList<>();

        toolbarHolder = (RelativeLayout) findViewById(R.id.contactUsToolbar);
        backButton = (ImageView) toolbarHolder.findViewById(R.id.toolbarBackButton);

        recyclerView = (RecyclerView) findViewById(R.id.contactListRCV);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        try {
            adapter = new _2013KUCP1013_ContactUs_ContactListAdapter(getFacultyListByJSONArray(jsonData) , getApplicationContext());
            adapter.setOnItemClickListener(this);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        recyclerView.setAdapter(adapter);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext() , _2013KUCP1013_ContactUs_MainActivity.class));
                finish();
            }
        });

    }


    public List<_2013KUCP1013_ContactUs_ContactListWrapper> getFacultyListByJSONArray(String jsonData) throws JSONException, IOException {

        JSONObject jsonRootObject = new JSONObject(jsonData);

        JSONArray jsonArray = jsonRootObject.getJSONArray("faculty");

        for (int i=0;i<jsonArray.length();i++){

            _2013KUCP1013_ContactUs_ContactListWrapper _2013KUCP1013_ContactUs_ContactListWrapper = new _2013KUCP1013_ContactUs_ContactListWrapper();

            JSONObject object = jsonArray.getJSONObject(i);
            String facultyName = object.getString("Name");
            String facultyContact = object.getString("Contact");
            String facultyImageLink = object.getString("ImageLink");

            _2013KUCP1013_ContactUs_ContactListWrapper.setFacultyName(facultyName);
            _2013KUCP1013_ContactUs_ContactListWrapper.setFacultyContact(facultyContact);
            _2013KUCP1013_ContactUs_ContactListWrapper.setFacultyImage(facultyImageLink);

            list.add(_2013KUCP1013_ContactUs_ContactListWrapper);

        }


        return list;
    }

    @Override
    public void onItemClick(View v, int position) {
        String contactNo = list.get(position).getFacultyContact();
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+91" + contactNo));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (checkCallingOrSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext() , _2013KUCP1013_ContactUs_MainActivity.class));
        finish();
    }
}
