package in.forsk.iiitk;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class _2013KUCP1013_ContactUs_MainActivity extends AppCompatActivity implements AsyncDelegate {

    private RelativeLayout callIconHolder;
    private RelativeLayout emailIconHolder;
    private RelativeLayout websiteIconHolder;
    private RelativeLayout locateUsIconHolder;
    static List<_2013KUCP1013_ContactUs_EmailDataWrapper> emailDataList;
    private String jsonUrl;
    private boolean isListFetched = false;
    private boolean isConnectionAvailable = false;
    private _2013KUCP1013_ContactUs_ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contactus_main_layout_2013kucp1013_contactus_);

        cd = new _2013KUCP1013_ContactUs_ConnectionDetector(getApplicationContext());
        isConnectionAvailable = cd.isConnectingToInternet();

        if (!isConnectionAvailable){
            showAlertDialog(this, "Internet Connection",
                    "No Internet connection");
        }

        jsonUrl = "http://online.mnit.ac.in/iiitk/assets/contact_us_json1";

        if (isConnectionAvailable) {
            FetchJSONInBackground background = new FetchJSONInBackground(this, this);
            background.execute(jsonUrl);
        }


        getInitialize();
        setUpListeners();

    }

    public void showAlertDialog(Context context, String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);

        alertDialog.setMessage(message);

        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alertDialog.show();
    }


    private void setUpListeners() {

        callIconHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isConnectionAvailable){
                    showAlertDialog(_2013KUCP1013_ContactUs_MainActivity.this , "Internet Connection" , "No Internet Connection");
                    return;
                }
                startActivity(new Intent(getApplicationContext(), _2013KUCP1013_ContactUs_ContactList.class));
                finish();
            }
        });

        emailIconHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isConnectionAvailable){
                    showAlertDialog(_2013KUCP1013_ContactUs_MainActivity.this , "Internet Connection" , "No Internet Connection");
                    return;
                }

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailDataList.get(0).getTo()});
                intent.putExtra(Intent.EXTRA_SUBJECT, emailDataList.get(0).getSubject());
                intent.putExtra(Intent.EXTRA_TEXT, emailDataList.get(0).getBody());


                try {
                    startActivity(Intent.createChooser(intent, "Choose Email Client : "));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        websiteIconHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.iiitkota.ac.in"));
                startActivity(webIntent);
            }
        });

        locateUsIconHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mnitLatitude = "" + 26.912434;
                String mnitLongitude = "" + 75.787271;

                String uri = "https://www.google.co.in/maps/place/Malaviya+National+Institute+of+Technology+Jaipur/@26.8625616,75.8183416,21z/data=!4m5!1m2!2m1!1smnit+jaipur!3m1!1s0x396db66fe2879c7f:0xdfc843bf9b6f869a";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));

                try {
                    startActivity(intent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Google-Maps Not installed.", Toast.LENGTH_LONG).show();
                }
            }
        });

    }


    private void getInitialize() {
        callIconHolder = (RelativeLayout) findViewById(R.id.callIconHolder);
        emailIconHolder = (RelativeLayout) findViewById(R.id.emailIconHolder);
        websiteIconHolder = (RelativeLayout) findViewById(R.id.websiteIconHolder);
        locateUsIconHolder = (RelativeLayout) findViewById(R.id.locatUsIconHolder);
        emailDataList = new ArrayList<>();
    }


    @Override
    public void asyncComplete(boolean success) {
        isListFetched = success;
    }

}

interface AsyncDelegate {

    public void asyncComplete(boolean success);

}

class FetchJSONInBackground extends AsyncTask<String  , Void , String>{


    private AsyncDelegate delegate;
    private Context context;
    private ProgressDialog progressDialog;

    public FetchJSONInBackground (AsyncDelegate delegate , Context c){
        this.delegate = delegate;
        context = c;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Fetching Data ... ");
        progressDialog.show();
        progressDialog.setCancelable(false);
    }

    @Override
    protected String doInBackground(String... params) {

        String jsonData = null;

        try {
            jsonData =  openHttpConnection(params[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonData;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        try {
            _2013KUCP1013_ContactUs_MainActivity.emailDataList = getEmailDataList(s);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        _2013KUCP1013_ContactUs_ContactList.jsonData = s;
        delegate.asyncComplete(true);

        progressDialog.hide();
    }

    private String openHttpConnection(String urlStr) throws IOException {
        InputStream in = null;
        int resCode = -1;

        try {
            URL url = new URL(urlStr);
            URLConnection urlConn = url.openConnection();

            if (!(urlConn instanceof HttpURLConnection)) {
                throw new IOException("URL is not an Http URL");
            }

            HttpURLConnection httpConn = (HttpURLConnection) urlConn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            resCode = httpConn.getResponseCode();
            if (resCode == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return convertStreamToString(in);
    }

    private String convertStreamToString(InputStream is) throws IOException {
        // Converting input stream into string
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int i = is.read();
        while (i != -1) {
            baos.write(i);
            i = is.read();
        }
        return baos.toString();
    }

    private List<_2013KUCP1013_ContactUs_EmailDataWrapper> getEmailDataList(String jsonDataString) throws JSONException, IOException {

        List<_2013KUCP1013_ContactUs_EmailDataWrapper> emailDataList = new ArrayList<>();

        JSONObject jsonRootObject = new JSONObject(jsonDataString);

        JSONObject object = jsonRootObject.getJSONObject("email");

        String to = object.getString("To");
        String subject = object.getString("Subject");
        String body = object.getString("Body");

        _2013KUCP1013_ContactUs_EmailDataWrapper _2013KUCP1013_ContactUs_EmailDataWrapper = new _2013KUCP1013_ContactUs_EmailDataWrapper();
        _2013KUCP1013_ContactUs_EmailDataWrapper.setTo(to);
        _2013KUCP1013_ContactUs_EmailDataWrapper.setSubject(subject);
        _2013KUCP1013_ContactUs_EmailDataWrapper.setBody(body);

        emailDataList.add(_2013KUCP1013_ContactUs_EmailDataWrapper);

        return emailDataList;
    }


}
