package in.forsk.iiitk;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by student on 28-Oct-15.
 */
public class _2013KUCP1013_ContactUs_ContactListAdapter extends RecyclerView.Adapter<_2013KUCP1013_ContactUs_ContactListAdapter.ViewHolder>{


    private List<_2013KUCP1013_ContactUs_ContactListWrapper> list;
    private Context context;
    private ItemClickListener itemClickListener;

    public _2013KUCP1013_ContactUs_ContactListAdapter(List<_2013KUCP1013_ContactUs_ContactListWrapper> facultyList , Context c) {
        list = facultyList;
        context = c;
    }

    public void setOnItemClickListener(ItemClickListener icl){
        itemClickListener = icl;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_row_2013kucp1013_contactus_ , parent , false);

        ViewHolder holder = new ViewHolder(rootView);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.facultyName.setText(list.get(position).getFacultyName());
        holder.facultyContact.setText(list.get(position).getFacultyContact());

        Picasso.with(context).load(list.get(position).getFacultyImage()).into(holder.facultyImage);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView facultyImage;
        TextView facultyName;
        TextView facultyContact;
        RelativeLayout dataHolder;

        public ViewHolder(View itemView) {
            super(itemView);

            facultyContact = (TextView) itemView.findViewById(R.id.facultyContact);
            facultyImage = (ImageView) itemView.findViewById(R.id.facultyImage);
            facultyName = (TextView) itemView.findViewById(R.id.facultyName);

            dataHolder = (RelativeLayout) itemView.findViewById(R.id.facultyDataHolder);

            dataHolder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (itemClickListener != null){
                        itemClickListener.onItemClick(v , getAdapterPosition());
                    }

                }
            });
        }
    }

    public interface ItemClickListener{
        void onItemClick(View v , int position);
    }
}


